<?=$this->extend('layouts/template')?>

<?=$this->section('content');?>
<section class="vh-100 gradient-custom">
  <div class="container py-5 h-100">
<?php if (session('error') !== null): ?>
                    <div class="alert alert-danger" role="alert"><?=session('error')?></div>
                <?php elseif (session('errors') !== null): ?>
                    <div class="alert alert-danger" role="alert">
                        <?php if (is_array(session('errors'))): ?>
                            <?php foreach (session('errors') as $error): ?>
                                <?=$error?>
                                <br>
                            <?php endforeach?>
                        <?php else: ?>
                            <?=session('errors')?>
                        <?php endif?>
                    </div>
                <?php endif?>
    <div class="row justify-content-center align-items-center h-100">
      <div class="col-12 col-lg-9 col-xl-7">
        <div class="card shadow-2-strong card-registration" style="border-radius: 15px;">
          <div class="card-body p-4 p-md-5">
            <h3 class="mb-4 pb-2 pb-md-0 mb-md-5">Registration Form</h3>
            <form action="<?=url_to('register')?>" method="POST">
            <?=csrf_field()?>
              <div class="row">
                <div class="col-md-6 mb-4">

                  <div class="form-outline">
                    <input type="text" id="firstName" class="form-control form-control-lg" name="first_name" required value="<?=old('first_name')?>" />
                    <label class="form-label" for="firstName">First Name</label>
                  </div>

                </div>
                <div class="col-md-6 mb-4">

                  <div class="form-outline">
                    <input type="text" id="lastName" class="form-control form-control-lg" name="last_name" value="<?=old('last_name')?>" required />
                    <label class="form-label" for="lastName">Last Name</label>
                  </div>

                </div>
              </div>

              <div class="row">
                <div class="col-md-6 mb-4 d-flex align-items-center">

                  <div class="form-outline w-100">
                    <input type="email" class="form-control form-control-lg" id="email" name="email" value="<?=old('email')?>" required />
                    <label for="email" class="form-label">Email</label>
                  </div>

                </div>
                <div class="col-md-6 mb-4">

                  <div class="form-outline">
                    <input type="text" id="username" class="form-control form-control-lg" name="username" required value="<?=old('username')?>" />
                    <label class="form-label" for="username">Username</label>
                  </div>

                </div>
              </div>

              <div class="row">
                <div class="col-md-6 mb-4 pb-2">

                  <div class="form-outline">
                    <input type="password" id="password" class="form-control form-control-lg" name="password" required/>
                    <label class="form-label" for="password">Password</label>
                  </div>

                </div>
                <div class="col-md-6 mb-4 pb-2">

                  <div class="form-outline">
                    <input type="password" id="phoneNumber" class="form-control form-control-lg" name="password_confirm" required/>
                    <label class="form-label" for="phoneNumber">Password Confirmation</label>
                  </div>

                </div>
              </div>
              <div class="mt-4 pt-2">
                <button class="btn btn-primary btn-lg" type="submit">Register</button>
              </div>

            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<?=$this->endSection();?>