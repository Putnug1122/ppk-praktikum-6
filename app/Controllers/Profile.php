<?php

namespace App\Controllers;

use App\Controllers\BaseController;

class Profile extends BaseController
{
    public function index()
    {
        // $data['user'] = auth()->user();
        // return view('profile', $data);
        if (auth()->loggedIn()) {
            $data['user'] = auth()->user();
            return view('profile', $data);
        } else {
            return redirect()->to('/login');
        }
    }
}
